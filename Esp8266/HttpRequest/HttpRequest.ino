
#include <ESP8266WiFi.h>

const char* ssid     = "Team2";
const char* password = "codeforgoodLaptop";

const char* host = "192.168.137.1";
const char* privateKey = "....................";
void  wifi_connect();

WiFiClient client;

const int httpPort = 80;

void setup() {
  //Serial.begin(9600);
  Serial.begin(115200);
  delay(10);
  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

unsigned long last_milis = 0;

void loop() {

  httpRequest("http://google.com");
  delay(100);
  
}
void httpRequest(String url)
{
  Serial.print("connecting to ");
  Serial.println(host);
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  Serial.print("Requesting URL: ");
  Serial.println(url);
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 1000) { //5000
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
   
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();
  Serial.println("closing connection"); 
}


