<!doctype html>
<?php
  function getPointsJson()
  {
    include ("database.php");
    $conn=openConnection();
    $points=readPointEntries($conn);
    
    return json_encode($points);
  }
  //echo getPointsJson();
?>
<html lang="en">
  <head>
    <link rel="stylesheet" href="https://openlayers.org/en/v4.5.0/css/ol.css" type="text/css">
    <style>
      .map {
        height: 400px;
        width: 100%;
      }
    </style>
    <script src="https://openlayers.org/en/v4.5.0/build/ol.js" type="text/javascript"></script>
    <script src="javascriptFunctions.js" type="text/javascript"></script>
    <title>OpenLayers example</title>
  </head>
  <body>
    <div id="map" class="map"></div>
    <script type="text/javascript">


    var pointsJson = httpRequestSync('/getMapData.php');
    var points = JSON.parse(pointsJson);

    var bingLayer = new ol.layer.Tile({
          visible: false,
          preload: Infinity,
          source: new ol.source.BingMaps({
            key: 'AooOFaixUfdTBP29KqbmLOXXpI7LJcJ9VnHomdG-REla4DpvRO33GrCKa3gQiUx5',
            imagerySet: 'AerialWithLabels'
            // use maxZoom 19 to see stretched tiles instead of the BingMaps
            // "no photos at this zoom level" tiles
            // maxZoom: 19
          })
        });
        bingLayer.setVisible(1);

        //default point
        var defaultPoint = [0, 51.5];
        if (undefined != points[0])
        {
          defaultPoint = [parseFloat(points[0]["Y"]),  parseFloat(points[0]["X"])];
        }

      var map = new ol.Map({
        target: 'map',
        layers: [bingLayer],
        view: new ol.View({
            center: ol.proj.fromLonLat(defaultPoint),
          zoom: 14
        })
      });
      //canary wharf [0, 51.5]


    var pointsConverted = [[0, 0]];

    for (var i = 0; i < points.length; i++) {
      var point = [parseFloat(points[i]["Y"]),  parseFloat(points[i]["X"])];
      //alert(point);
      //break;
        pointsConverted[i] = ol.proj.transform(point, 'EPSG:4326', 'EPSG:3857');
    }


    var featureLine = new ol.Feature({
        geometry: new ol.geom.LineString(pointsConverted)
    });

    var vectorLine = new ol.source.Vector({});
    vectorLine.addFeature(featureLine);

    var vectorLineLayer = new ol.layer.Vector({
        source: vectorLine,
        style: new ol.style.Style({
            fill: new ol.style.Fill({ color: '#00FF00', weight: 4 }),
            stroke: new ol.style.Stroke({ color: '#00FF00', width: 2 })
        })
    });
    map.addLayer(vectorLineLayer);
    </script>
  </body>
</html>