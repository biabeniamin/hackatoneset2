<html>
    <body>
<script src="javascriptFunctions.js"></script>
<script>
    function deactivateSubmit()
    {
        document.forms["login"]["submit"].style.display = "none";
    }
    function activateSubmit()
    {
        document.forms["login"]["submit"].style.display = "";
    }
    function emailInputChanged()
    {
        var email = document.forms["login"]["email"].value;
        emailStatus = checkEmail(email);
        document.getElementById("emailStatus").innerHTML = emailStatus;
        if("" != emailStatus)
        {
            deactivateSubmit();
        }
        else
        {
            activateSubmit();
        }
    }
</script>
</body>
<?php
if("POST" == $_SERVER["REQUEST_METHOD"])
{
    require 'database.php';
    $result = 0;
    if(isset($_POST['email']))
    {
        if(isset($_POST['password']))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $conn = openConnection();
            $result = checkUserExists($conn, $email, $password);
            if("1" == $result)
            {
                echo "Login success!";
                die();
            }
            else
            {
                echo "cannot log in";
            }

        }
    }

    echo $result;
}
?>
<div id="demo"></div>

<form name="login" action="index.php" method="POST">
<div style="display:inline;">
Email:
<input onchange="emailInputChanged()" type="text" name="email">
<h4><div id="emailStatus"></div></h4><br>
Password:
<input type="password" name="password">
<div id="passwordStatus"></div><br>
<input type="submit" name="submit">
</form>