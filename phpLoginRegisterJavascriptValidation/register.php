<html>
    <body>
<script src="javascriptFunctions.js"></script>
<script>
    function deactivateSubmit()
    {
        document.forms["login"]["submit"].style.display = "none";
    }
    function activateSubmit()
    {
        document.forms["login"]["submit"].style.display = "";
    }
    function emailInputChanged()
    {
        var email = document.forms["login"]["email"].value;
        emailStatus = checkEmail(email, "emailStatus");
        document.getElementById("emailStatus").innerHTML = emailStatus;
        if("" != emailStatus)
        {
            deactivateSubmit();
        }
        else
        {
            activateSubmit();
        }
    }
    function passwordInputChanged()
    {
        var password = document.forms["login"]["password"].value;
        var passwordStatus = checkPassword(password);
        document.getElementById("passwordStatus").innerHTML = passwordStatus;
        if("" != passwordStatus)
        {
            deactivateSubmit();
        }
        else
        {
            activateSubmit();
        }
    }
</script>
</body>
<?php
if("POST" == $_SERVER["REQUEST_METHOD"])
{
    require 'database.php';
    $result = 0;
    if(isset($_POST['email']))
    {
        if(isset($_POST['password']))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $conn = openConnection();
            $result = checkEmail($conn, $email);
            if("1" == $result)
            {
                echo "Email already exists";
                die();
            }

            $result = addNewUser($conn, $email, $password);
            if("1" == $result)
            {
                echo "The account was created!";
            }
            else
            {
                echo "The account cannnot be created";
            }
        }
    }

    echo $result;
}
?>
<div id="demo"></div>

<form name="login" action="register.php" method="POST">
<div style="display:inline;">
Email:
<input onchange="emailInputChanged()" type="text" name="email">
<h4><div id="emailStatus"></div></h4><br>
Password:
<input onchange="passwordInputChanged()" type="password" name="password">
<div id="passwordStatus"></div><br>
<input type="submit" name="submit">
</form>